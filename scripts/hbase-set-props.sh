#!/bin/sh

. $(dirname "${0}")/set-pax-flag-of-java.sh

# the functions require GNU sed (the usage of busybox sed results into incorrect outputs)

addHBaseProperty() {
	local path="${1}"
	local name="${2}"
	local value="${3}"
	local entry="<property><name>${name}</name><value>${value}</value></property>"
	local escapedEntry=$(echo "${entry}" | sed 's/\//\\\//g')
	sed -i "/<\/configuration>/ s/.*/${escapedEntry}\n&/" "${path}"
}

cleanHBaseProperties() {
	local path="${1}"
	if [ -e "${path}" ]; then
		sed -i '/^<configuration>/,/^<\/configuration>/{//!d}' "${path}"
	else
		cat >"${path}" <<END
<?xml version="1.0"?>
<configuration>
</configuration>
END
	fi
}

reconfHBaseByEnvVars() {
	local path="${1}"
	local envPrefix="${2}"
	echo "* reconfiguring ${path}"
	cleanHBaseProperties "${path}"
	for I in `printenv | grep "^${envPrefix}_[^=]*="`; do
		local name=`echo "${I}" | sed -e "s/^${envPrefix}_\\([^=]*\\)=.*\$/\\1/" -e 's/___/-/g' -e 's/__/_/g' -e 's/_/./g'`
		local value="${I#*=}"
		echo "** setting ${name}=${value}"
		addHBaseProperty "${path}" "${name}" "${value}"
	done
}

reconfHBaseByEnvVars "${HADOOP_CORE_CONF}" PROP_CORE
reconfHBaseByEnvVars "${HBASE_SITE_CONF}" PROP_SITE
reconfHBaseByEnvVars "${HBASE_POLICY_CONF}" PROP_POLICY
