#!/bin/sh

# from docker-hbase/scripts/application-helpers.sh
. $(dirname "${0}")/application-helpers.sh

# For HBase properties, see
# https://github.com/apache/hbase/blob/master/hbase-common/src/main/resources/hbase-default.xml

# Functions to set the HDFS properties, see
# https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/hdfs-default.xml

set_distributed() {
	if [ -z "${PROP_SITE_hbase_cluster_distributed}" ]; then
		export PROP_SITE_hbase_cluster_distributed="true"
	fi
	if [ -z "${HBASE_MANAGES_ZK}" ]; then
		export HBASE_MANAGES_ZK="false"
	fi
}

set_root_dir() {
	if [ -n "${PROP_SITE_hbase_rootdir}" ]; then
		export ROOT_DIR="${PROP_SITE_hbase_rootdir}"
	else
		export ROOT_DIR="${ROOT_DIR:-/tmp}"
		export PROP_SITE_hbase_rootdir="${ROOT_DIR}"
	fi
	# the HDFS root dir will be set also as default FS for HDFS components, e.g., for org.apache.hadoop.fs.FsShell
	if [ -z "${ROOT_DIR##hdfs://*}" -a -z "${PROP_CORE_fs_defaultFS}" ]; then
		ROOT_DIR_FS=$(echo "${ROOT_DIR}" | cut -d / -f 1-3)
		export PROP_CORE_fs_defaultFS="${ROOT_DIR_FS}"
	fi
}

set_master_ports() {
	# The port the HBase Master should bind to
	if [ -n "${PROP_SITE_hbase_master_port}" ]; then
		export MASTER_PORT="${PROP_SITE_hbase_master_port}"
	else
		export MASTER_PORT="${MASTER_PORT:-16000}"
		export PROP_SITE_hbase_master_port="${MASTER_PORT}"
	fi
	# The port for the HBase Master web UI
	if [ -n "${PROP_SITE_hbase_master_info_port}" ]; then
		export MASTER_INFO_PORT="${PROP_SITE_hbase_master_info_port}"
	else
		export MASTER_INFO_PORT="${MASTER_INFO_PORT:-16010}"
		export PROP_SITE_hbase_master_info_port="${MASTER_INFO_PORT}"
	fi
}

set_regionserver_ports() {
	# The port the HBase RegionServer binds to
	if [ -n "${PROP_SITE_hbase_regionserver_port}" ]; then
		export REGIONSERVER_PORT="${PROP_SITE_hbase_regionserver_port}"
	else
		export REGIONSERVER_PORT="${REGIONSERVER_PORT:-16020}"
		export PROP_SITE_hbase_regionserver_port="${REGIONSERVER_PORT}"
	fi
	# The port for the HBase RegionServer web UI
	if [ -n "${PROP_SITE_hbase_regionserver_info_port}" ]; then
		export REGIONSERVER_INFO_PORT="${PROP_SITE_hbase_regionserver_info_port}"
	else
		export REGIONSERVER_INFO_PORT="${REGIONSERVER_INFO_PORT:-16030}"
		export PROP_SITE_hbase_regionserver_info_port="${REGIONSERVER_INFO_PORT}"
	fi
}

# For ZooKeeper properties, see
# https://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_configuration

set_zoo_dirs() {
	# data dir
	if [ -n "${PROP_SITE_hbase_zookeeper_property_dataDir}" ]; then
		export ZOO_DATA_DIR="${PROP_SITE_hbase_zookeeper_property_dataDir}"
	fi
	if [ -n "${ZOO_DATA_DIR}" ]; then
		mkdir -vp "${ZOO_DATA_DIR}"
		chmod -v 700 "${ZOO_DATA_DIR}"
		if [ ! -f "${ZOO_DATA_DIR}/myid" ]; then
			echo "${ZOO_MY_ID:-1}" > "${ZOO_DATA_DIR}/myid"
		fi
		chown -R "${HBASE_USER}:${HBASE_USER}" "${ZOO_DATA_DIR}"
		export PROP_SITE_hbase_zookeeper_property_dataDir="${ZOO_DATA_DIR}"
	fi
	# data-log dir
	if [ -n "${PROP_SITE_hbase_zookeeper_property_dataLogDir}" ]; then
		export ZOO_DATA_LOG_DIR="${PROP_SITE_hbase_zookeeper_property_dataLogDir}"
	fi
	if [ -n "${ZOO_DATA_LOG_DIR}" ]; then
		mkdir -vp "${ZOO_DATA_LOG_DIR}"
		chmod -v 700 "${ZOO_DATA_LOG_DIR}"
		chown -R "${HBASE_USER}:${HBASE_USER}" "${ZOO_DATA_LOG_DIR}"
		export PROP_SITE_hbase_zookeeper_property_dataLogDir="${ZOO_DATA_LOG_DIR}"
	fi
}

set_zoo_quorum() {
	# The following three properties are used together to create the list of host:peer_port:leader_port quorum servers for ZooKeeper.
	# hosts
	if [ -n "${PROP_SITE_hbase_zookeeper_quorum}" ]; then
		export ZOO_SERVERS="${PROP_SITE_hbase_zookeeper_quorum}"
	else
		export ZOO_SERVERS="${ZOO_SERVERS:-localhost}"
		export PROP_SITE_hbase_zookeeper_quorum="${ZOO_SERVERS}"
	fi
	# peer
	if [ -n "${PROP_SITE_hbase_zookeeper_peerport}" ]; then
		export ZOO_PEERPORT="${PROP_SITE_hbase_zookeeper_peerport}"
	else
		export ZOO_PEERPORT="${ZOO_PEERPORT:-2888}"
		export PROP_SITE_hbase_zookeeper_peerport="${ZOO_PEERPORT}"
	fi
	# leader
	if [ -n "${PROP_SITE_hbase_zookeeper_leaderport}" ]; then
		export ZOO_LEADERPORT="${PROP_SITE_hbase_zookeeper_leaderport}"
	else
		export ZOO_LEADERPORT="${ZOO_LEADERPORT:-3888}"
		export PROP_SITE_hbase_zookeeper_leaderport="${ZOO_LEADERPORT}"
	fi
	# client
	if [ -n "${PROP_SITE_hbase_zookeeper_property_clientPort}" ]; then
		export ZOO_PORT="${PROP_SITE_hbase_zookeeper_property_clientPort}"
	else
		export ZOO_PORT="${ZOO_PORT:-2181}"
		export PROP_SITE_hbase_zookeeper_property_clientPort="${ZOO_PORT}"
	fi
}

# make HDFS root directory for HBase
make_root_dir() {
	if [ -z "${ROOT_DIR##hdfs://*}" ]; then
		# make_hdfs_dir is from docker-hbase/scripts/application-helpers.sh
		make_hdfs_dir "${ROOT_DIR}?user=${HBASE_USER}&group=${HBASE_USER}&perms=1777"
	fi
}
