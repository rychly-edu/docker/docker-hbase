#!/bin/sh

DIR=$(dirname "${0}")

if [ -n "${ROLE}" ]; then
	# "execute", not "source" as it is a bash script while we are in the dash shell
	exec "${DIR}/entrypoint-${ROLE}.sh" ${@}
else
	. "${DIR}/hbase-entrypoint-helpers.sh"
	. "${DIR}/hbase-set-props.sh"
	exec ${@}
fi
