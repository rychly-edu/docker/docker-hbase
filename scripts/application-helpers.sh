#!/bin/sh

# wait for sockets, e.g., WAIT_FOR=server1:port1@timeout1,server2:port2@timeout2
wait_for() {
	local I
	for I in $(echo "${WAIT_FOR}" | tr ',' ' '); do
		wait-for $(echo "${I}" | sed 's/@/ --timeout=/')
	done
}

# make a HDFS directory, e.g., hdfs://hostname:port/path/directory1?user=user1&group=group1&perms=perms1
make_hdfs_dir() {
	# parse params
	local DIR_FS_PARAM=""
	local DIR_PATH=""
	local DIR_USER=""
	local DIR_GROUP=""
	local DIR_PERMS=""
	local OLD_IFS="${IFS}"
	local IFS="${IFS}?&"
	local I
	for I in $@; do
		case "${I}" in
			user=*)
				DIR_USER="${I:5}"
			;;
			group=*)
				DIR_GROUP="${I:6}"
			;;
			perms=*)
				DIR_PERMS="${I:6}"
			;;
			*=*)
				echo "Unknown argument for make_hdfs_dir(): ${I}" >&2
				exit 1
			;;
			hdfs://*)
				DIR_FS_PARAM="-fs $(echo "${I}" | cut -d / -f 1-3)/"
				DIR_PATH=/$(echo "${I}" | cut -d / -f 4-)
			;;
			*)
				DIR_PATH="${I}"
			;;
		esac
	done
	IFS="${OLD_IFS}"
	# make directory
	local HDFS_DFS="${HBASE_HOME}/bin/hbase --config ${HBASE_CONF_DIR} org.apache.hadoop.fs.FsShell"
	# HADOOP_USER_NAME cannot be set to "hadoop" and exported, as it would affect also later instances of spark
	echo HADOOP_USER_NAME=${HADOOP_USER_NAME:-hadoop} ${HDFS_DFS} ${DIR_FS_PARAM} -mkdir -p "${DIR_PATH}"
	if [ -n "${DIR_USER}" -o -n "${DIR_GROUP}" ]; then
		echo HADOOP_USER_NAME=${HADOOP_USER_NAME:-hadoop} ${HDFS_DFS} ${DIR_FS_PARAM} -chown -R "${DIR_USER}:${DIR_GROUP}" "${DIR_PATH}"
	fi
	if [ -n "${DIR_PERMS}" ]; then
		echo HADOOP_USER_NAME=${HADOOP_USER_NAME:-hadoop} ${HDFS_DFS} ${DIR_FS_PARAM} -chmod "${DIR_PERMS}" "${DIR_PATH}"
	fi
}

# make a HDFS directories, e.g., MAKE_HDFS_DIRS=hdfs://hostname:port/path/directory1?user=user1&group=group1&perms=perms1,hdfs://hostname:port/path/directory1?user=user1&group=group1&perms=perms1
make_hdfs_dirs() {
	local I
	for I in $(echo "${MAKE_HDFS_DIRS}" | tr ',' ' '); do
		make_hdfs_dir "${I}"
	done
}
