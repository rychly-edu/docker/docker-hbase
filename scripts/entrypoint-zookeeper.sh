#!/bin/sh

DIR=$(dirname "${0}")

. "${DIR}/hbase-entrypoint-helpers.sh"

# from docker-hbase/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_zoo_quorum
set_zoo_dirs

. "${DIR}/hbase-set-props.sh"

wait_for

exec su "${HBASE_USER}" -c "exec ${HBASE_HOME}/bin/hbase --config ${HBASE_CONF_DIR} zookeeper ${@} start"
