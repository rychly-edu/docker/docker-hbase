#!/usr/bin/env bash

DIR=$(dirname "${0}")

. "${DIR}/hbase-entrypoint-helpers.sh"

# from docker-hbase/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_distributed
set_zoo_quorum
set_root_dir
set_master_ports

. "${DIR}/hbase-set-props.sh"

wait_for
make_root_dir

exec su "${HBASE_USER}" -c "exec ${HBASE_HOME}/bin/hbase --config ${HBASE_CONF_DIR} master ${@} start"
