#!/bin/sh

case "${ROLE}" in
	master|master-backup)
		URL="http://localhost:${MASTER_INFO_PORT:-16010}/master-status"
		if which wget; then
			wget -qO /dev/null "${URL}" || exit 1
		elif which curl; then
			curl -LfsSo /dev/null "${URL}" || exit 1
		else
			echo "Missing wget or curl, cannot perform the health-check." >&2
			exit 0
		fi
		;;
	regionserver)
		URL="http://localhost:${REGIONSERVER_INFO_PORT:-16030}/rs-status"
		if which wget; then
			wget -qO /dev/null "${URL}" || exit 1
		elif which curl; then
			curl -LfsSo /dev/null "${URL}" || exit 1
		else
			echo "Missing wget or curl, cannot perform the health-check." >&2
			exit 0
		fi
		;;
	zookeeper)
		exec su "${HBASE_USER}" -c '${HBASE_HOME}/bin/hbase --config ${HBASE_CONF_DIR} zkcli -server localhost:${ZOO_PORT} stat / || exit 1'
		;;
	*)
		echo "Unknow role '${ROLE}', cannot perform the health-check." >&2
		exit 0
esac
