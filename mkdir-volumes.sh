#!/bin/sh

exec mkdir -vp volumes/zk-data volumes/zk-data-log volumes/namenode volumes/namenode-secondary $(seq -f 'volumes/datanode%g' -s ' ' 1 10)
