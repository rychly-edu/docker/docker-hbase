# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://hbase.apache.org/downloads.html
2.2.0	HBASE_VERSION=2.2.0
