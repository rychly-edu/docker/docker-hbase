# Apache HBase Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-hbase/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-hbase/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-hbase/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-hbase/commits/master)

This is just a base image for building another docker images!

The image is based on [adoptopenjdk/openjdk8](https://hub.docker.com/r/adoptopenjdk/openjdk8)

*	[adoptopenjdk/openjdk8:alpine-slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim)
*	[adoptopenjdk/openjdk8:slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/ubuntu/Dockerfile.hotspot.releases.slim)

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-hbase:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-hbase" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run

~~~sh
docker run -ti "registry.gitlab.com/rychly-edu/docker/docker-hbase:latest" /bin/sh --login
~~~

## Configuration

TBA
